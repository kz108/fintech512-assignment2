var ready = (callback) => {
    if (document.readyState !== "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  };
  
  ready(() => {
    /* Register event listeners */
    document.getElementById("stock-select").addEventListener("change", updatePlot);
    document.getElementById("submit-button").addEventListener("click", () => {
      bootbox.confirm({
        message: "Do you want to submit the page?",
        buttons: {
          confirm: {
            label: "Yes",
            className: "btn-success",
          },
          cancel: {
            label: "No",
            className: "btn-danger",
          },
        },
        callback: (result) => {
          if (result) {
            bootbox.alert({
              message: "Form Submitted",
              size: "small",
            });
          } else {
            bootbox.alert({
              message: "Action Cancelled",
              size: "small",
            });
          }
        },
      });
    });
  });
  
function updatePlot() {
    var selectedStock = document.getElementById("stock-select").value;
    if (selectedStock === "") {
        return;
    }

    fetch("data/" + selectedStock + ".csv")
        .then((response) => response.text())
        .catch(error => {
            alert(error)
        })
        .then((text) => {
            csv().fromString(text).then(processData);
        });
}

function makeplot() {
    updatePlot();
};


function processData(data) {
    let x = [], y = [];

    for (let i = 0; i < data.length; i++) {
        row = data[i];
        x.push(row['Date']);
        y.push(row['Close']);
    }
    makePlotly(x, y);
}

function makePlotly(x, y) {
    var traces = [{
        x: x,
        y: y
    }];
    var stockname = document.querySelector("#stock-select option:checked").text;
    var layout = { title: stockname + " Stock Price History" };
    myDiv = document.getElementById('myDiv');
    Plotly.newPlot(myDiv, traces, layout);
};